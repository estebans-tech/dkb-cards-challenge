// https://docs.cypress.io/api/introduction/api.html

describe('DKB Cards Page', () => {
  it('visits the app root url', () => {
    cy.visit('http://localhost:3000/')
  })

  it('has Private Card', () => {
    cy.visit('http://localhost:3000/')
      .get('a')
      .should('contain.text', 'Private Card')
  })

  it('has Business Card', () => {
    cy.visit('http://localhost:3000/')
      .get('a')
      .should('contain.text', 'Business Card')
  })

  it('should go to Business Card page', () => {
    cy.visit('http://localhost:3000/')
      .get('a>div')
      .contains('Business Card')
      .click()
  })
})
