import { defineStore } from 'pinia'
import type { ICard } from '@/types'

export const useCardStore = defineStore({
  id: 'cards',
  state: () => ({
    businnessCards: [] as string[],
    cards: [] as ICard[]
  }),
  getters: {
    isBusiness: (state) => {
      return (id: string) =>
        state.businnessCards.find((it: string) => it === id)
    },
    getCards: (state) => state.cards
  },
  actions: {
    addBusinessCard(id: string) {
      const check = this.businnessCards.find((it: string) => it === id)
      if (!check) {
        this.businnessCards.push(id)
      }
    },
    setCards(items: ICard[]) {
      this.cards = items
    },
    getCardById (id: string) {
      return this.cards.find((it) => it.id === id)
    }
  }
})
