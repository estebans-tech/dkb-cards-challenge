import { ref } from 'vue'
import type { ICard } from '@/types'

const useCards = () => {
  // Data
  const cards = ref<ICard[]>([])
  const error = ref<string | null>(null)
  const isLoading = ref(true)

  // Methods
  const loadCards = async () => {
    try {
      const data = await fetch('/data/cards.json')

      if (!data.ok) {
        throw new Error('No data found')
      }

      cards.value = await data.json()
    } catch (err: unknown) {
      if (err instanceof Error) {
        error.value = err.message
      }
    } finally {
      isLoading.value = false
    }
  }

  /**
   * Find term in the description property.
   *
   * @param items {ICard} - The Card Object.
   * @param searchTerm {string} - The term to find.
   * @returns { boolean }
   */
  const findInDescription = (items: ICard, searchTerm: string): boolean => {
    const check = items.description
      .toLowerCase()
      .includes(searchTerm.toLowerCase())

    return check
  }

  return {
    cards,
    error,
    loadCards,
    isLoading,
    findInDescription
  }
}

export default useCards
