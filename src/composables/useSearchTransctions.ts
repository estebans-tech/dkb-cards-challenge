import { ref, computed, watch, type Ref } from 'vue'
import type { ITransaction } from '@/types'
export type searchPropTypes = 'amount' | 'description'

const useSearchTransctions = (
  list: Ref<ITransaction[]>,
  searchProp: searchPropTypes
) => {
  // Data
  const filteredItems = ref<ITransaction[]>([])
  const activeSearchTerm = ref('')
  const enteredSearchTerm = ref('')

  // Computed
  const availableAmountItems = computed(() => {
    if (activeSearchTerm.value && !isNaN(Number(activeSearchTerm.value))) {
      filteredItems.value = list.value.filter(
        (it) => it[searchProp] >= Number(activeSearchTerm.value)
      )
    } else {
      filteredItems.value = list.value
    }

    return filteredItems.value
  })

  // Watcher
  watch(enteredSearchTerm, (newValue: string) => {
    setTimeout(() => {
      if (newValue === enteredSearchTerm.value) {
        activeSearchTerm.value = newValue
      }
    }, 200)
  })

  // Methods
  const updateSearch = (val: string) => {
    enteredSearchTerm.value = val
  }

  return {
    availableAmountItems,
    updateSearch
  }
}

export default useSearchTransctions
