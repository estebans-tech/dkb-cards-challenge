import { ref } from 'vue'
import type { ITransaction } from '@/types'

const useCardTransactions = (id: string) => {
  // Data
  const transactions = ref<ITransaction[]>([])
  const error = ref<string | null>(null)
  const isLoading = ref(true)

  // Methods
  const loadTransactions = async () => {
    try {
      const data = await fetch('/data/transactions.json')

      if (!data.ok) {
        throw new Error('No data found')
      }

      const result = await data.json()

      if (result[id]) {
        transactions.value = result[id]
      }
    } catch (err: unknown) {
      if (err instanceof Error) {
        error.value = err.message
      }
    } finally {
      isLoading.value = false
    }
  }

  return {
    transactions,
    error,
    loadTransactions,
    isLoading
  }
}

export default useCardTransactions
