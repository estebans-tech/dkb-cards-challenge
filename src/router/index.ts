import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/CardsView.vue')
    },
    {
      path: '/card',
      name: 'card',
      redirect: '/'
    },
    {
      path: '/card/:id',
      name: 'card-detail',
      component: () => import('../views/CardView.vue'),
      props: true
    }
  ]
})

export default router
