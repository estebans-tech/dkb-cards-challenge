import { describe, it, expect, beforeEach } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'

import { mount } from '@vue/test-utils'
import CardItem from '@/components/CardItem.vue'

describe('CardItem', () => {
  beforeEach(() => {
    // creates a fresh pinia and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia())
  })

  it('renders properly', () => {
    const data = { id: 'lkmfkl-mlfkm-dlkfm', description: 'Private Card' }
    const wrapper = mount(CardItem, {
      props: {
        data
      }
    })
    const a = wrapper.find('div')
    expect(a.text()).toBe(data.description)
  })
})
