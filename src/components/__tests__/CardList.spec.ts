import { describe, it, expect, beforeEach } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'

import { mount } from '@vue/test-utils'
import CardList from '@/components/CardList.vue'

describe('CardList', () => {
  beforeEach(() => {
    // creates a fresh pinia and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia())
  })

  it('renders properly', () => {
    const wrapper = mount(CardList, {
      props: {
        data: [{ id: 'lkmfkl-mlfkm-dlkfm', description: 'Private Card' }]
      }
    })

    expect(wrapper.findAll('routerlink').length).toBe(1)
  })

  it('has no router links', () => {
    const wrapper = mount(CardList)

    expect(wrapper.findAll('routerlink').length).toBe(0)
  })
})
