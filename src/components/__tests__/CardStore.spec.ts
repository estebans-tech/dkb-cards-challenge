// counterStore.spec.ts
import { describe, it, expect, beforeEach } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'
import { useCardStore } from '@/stores/card'

describe('Counter Store', () => {
  const cardData = { id: 'elek-n3lk-4m3lk4', description: 'Business Card' }
  beforeEach(() => {
    // creates a fresh pinia and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia())
  })

  it('increments cards', () => {
    const card = useCardStore()
    expect(card.cards.length).toBe(0)
    card.setCards([cardData])
    expect(card.cards.length).toBe(1)
  })

  it('has no business cards', () => {
    const card = useCardStore()
    expect(card.isBusiness(cardData.id)).toBeFalsy()
  })

  it('has business cards', () => {
    const card = useCardStore()
    card.addBusinessCard(cardData.id)
    expect(card.isBusiness('fake-id')).toBeFalsy()
  })

  it('has business cards', () => {
    const card = useCardStore()
    card.addBusinessCard('some-id')
    expect(card.isBusiness(cardData.id)).toBeFalsy()
  })
})
