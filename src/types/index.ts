/**
 * Card Interface
 */
export interface ICard {
  id: string
  description: string
}

/**
 * Cart Transaction Interface
 */
export interface ITransaction {
  id: string
  amount: number
  description: string
}
